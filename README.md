# huaweicloud-solution-ploto  
PLOTO：自动驾驶研发平台，集合业界优秀伙伴提供专业的自动驾驶研发工具。  
前端使用vue框架，后端采用Django框架。  
为自动驾驶全链路提供可靠服务（数据采集、数据传输、数据脱敏、场景数据提取、数据标注、AI训练、仿真测试、评估评价、OTA升级全链路流程）。  

## 组织结构

```
├── huaweicloud-solution-ploto --主目录
    ├── config
    ├── initutils			--初始化项目目录	
    ├── ploto				--后端项目目录
    |  ├── ae_trace			--事件告警功能目录
    |  ├── commmon			--通用函数工具
    |  ├── conf				--配置目录
    |  ├── data_mgt			--数据管理目录
    |  ├── logs				--日志文件目录
    |  ├── monitor			--大屏数据目录
    |  ├── ploto			--ploto应用目录
    |  ├── __init__.py		--模块声明文件
    |  ├── user				--用户管理目录
    |  ├── manage.py		--Django管理文件
    |  └── requirements.txt --python环境依赖列表
    ├── src					--前端源码目录			
    ├── public
    ├── vue.config.js
    └── README.md
```
## 架构图

![image-20221010191916938](image/image-20221010191916938.png)



# server环境配置

Python版本：Python 3.7及以上版本

运行环境：CentOS 7.6



## 环境安装

### **安装python 3.7**

- 安装python编译相关的依赖包

```shell
yum install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gcc make libffi-devel

yum -y install epel-release 
yum install python-pip
yum install -y python3-pip
pip install wget
```

- 下载python3.7的源码

```shell
wget https://mirrors.huaweicloud.com/python/3.7.2/Python-3.7.2.tgz
```

- 编译生成

```shell
# 解压缩
tar -zxvf Python-3.7.2.tgz
# 进入解压后的目录
cd Python-3.7.2
# 手动编译
./configure prefix=/usr/local/python3 
make && make install
```

- 添加软连接

```shell
ln -s /usr/local/python3/bin/python3.7 /usr/bin/python3
ln -s /usr/local/python3/bin/pip3.7 /usr/bin/pip3
```

- 测试是否安装成功

```
[root@loaclhost ~]# python3 --version
Python 3.7.2
[root@loaclhost ~]# pip3 --version
pip 10.0.1 
```



### **环境依赖安装**

- cd到项目**主目录/ploto**下，找到requirements.txt，使用批量安装的方式安装python相关的环境依赖

```
pip3 install -r requirements.txt
```



### 功能组件安装

- Redis：用于当作Monitor查询的缓存数据，Celery的任务队列，以及登录查询和告警相关功能

  推荐使用华为云**分布式缓存服务DCS**服务快速部署，操作指引参考https://support.huaweicloud.com/qs-dcs/index.html

- Mysql，用于存储平台的用户、车辆、任务等相关信息

  推荐使用华为**云数据库RDS**快速部署，操作指引参考https://support.huaweicloud.com/qs-rds/zh-cn_topic_0046585334.html

- ElasticSesrch：用于加速事件告警功能的查询

  推荐使用华为**云搜索服务CSS**，操作指引参考https://support.huaweicloud.com/qs-css/index.html#toTop

- 安装node.js，用于在服务器运行前端环境

  下载文件并解压，推荐使用华为云镜像加速https://repo.huaweicloud.com/nodejs/

  ```shell
  wget https://repo.huaweicloud.com/nodejs/v16.16.0/node-v16.16.0-linux-arm64.tar.gz
  tar -xzf node-v16.16.0-linux-arm64.tar.gz
  ```

  设置软连接（根据实际安装的node路径修改），连接到/usr/local/bin/

  ```shell
  ln -s /root/node-v16.16.0-linux-x64/bin/npm /usr/local/bin/
  ln -s /root/node-v16.16.0-linux-x64/bin/node /usr/local/bin/
  ```

  在/root/目录下验证是否安装成功，打印出版本号则安装成功

  ```shell
  node -v
  npm -v
  ```

- 安装nginx，用于转发前端收到的请求

  ```
  yum install nginx
  ```

  

## 前后端部署

### 1、前端部署

#### **1.1 配置Node.js**

- 切换到主目录**huaweicloud-solution-ploto**执行

  ```
  npm install
  ```

- 修改服务器地址信息

  在主目录下**vue.config.js**中host修改前端服务器所在的地址，端口默认为80

  ```javascript
  devServer: {
      host: '127.0.0.1',  # 前端服务器地址,若为本机运行则使用本机地址
      port: '80',
      open: true
  }
  ```

- 主目录下编译运行，生成**dist**文件（任何对前端文件的修改都需要重新编译生成）

  ```shell
  npm run build
  ```
  

#### **1.2 配置Nginx**

- 若nginx安装路径为**/usr/local**目录下，修改 **/usr/local/nginx/conf/nginx.conf**配置文件，监听80端口的服务，**location/**中root对应build之后生成的dist文件目录，将前端的访问路径通过nginx代理转发到后端服务的8000端口


  ```
  server {
          listen       80;
          server_name  localhost;
          location / {
          	root  /root/huaweicloud-solution-ploto/dist/; # 修改为上一步生成的dist目录
          	index  index.html index.htm;
          }
          
          location /login{
          	proxy_pass http://localhost:8000;
        	}
  
        	location /monitor{
          	proxy_pass http://localhost:8000;
        	}
  
          location /data{
          	proxy_pass http://localhost:8000;
          }
  
          location /ADlogin{
          	proxy_pass http://localhost:8000;
          }
  
          location /logout{
          	proxy_pass http://localhost:8000;
          }
  
          location /admin{
          	proxy_pass http://localhost:8000;
          }
  
          error_page   500 502 503 504  /50x.html;
          	location = /50x.html {
          	root   html;
          }
  }
  ```

- 启动或重启nginx使配置生效


  ```
  /usr/local/nginx/sbin/nginx start
  /usr/local/nginx/sbin/nginx -s reload
  ```
### 2、后端部署

#### 2.1 配置连接信息

1. 在**ploto/ploto/settings/dev.py**中填入数据库，Redis，ElasticSearch的连接信息

   其中DATABASES 为数据库配置信息，先在mysql中创建一个数据库ploto_test，作为项目使用的数据库，并将其配置参数添加至字段中

   ```python
   # 数据库配置
   DATABASES = {
       "default": {
           "ENGINE": "django.db.backends.mysql",
           "NAME": "ploto_test",	# 创建的数据库名
           "USER": "root",			# 用户名默认为root		
           "PASSWORD": "****",		# 数据库登录密码
           "HOST": "127.0.0.1",	# 数据库服务器地址
           "PORT": "3306",			# 数据库连接端口，默认3306
       }
   }
   
   # ElasticSearch 配置
   ES_HOST = '127.0.0.1'			# CSS服务中的服务器地址
   ES_PORT = '9200'				# 服务器连接端口，默认9200
   ES_USERNAME = '*****'			
   ES_PASSWORD = '*****'
   
   # Redis 配置
   REDIS_HOST = '127.0.0.1'		# Redis服务器地址
   REDIS_PORT = '6379'				# Redis服务器连接端口，默认6379
   REDIS_PASSWORD = '*****'
   ```

   

2. **配置OBS连接信息**

   存放海量自动驾驶数据，推荐使用华为云**对象存储服务OBS**，操作指引https://support.huaweicloud.com/qs-obs/obs_qs_1000.html

   - 在华为云的控制台中的用户名下拉菜单中->“ 我的凭证”，在访问密钥中新增访问密钥，获得Access Key和Secret Key（请妥善保管并定期更新）

   - 在华为云的对象存储服务OBS中，创建一个桶（桶名为ploto-test）
   
   - 在**ploto/conf/conf.py**填写访问的ak，sk，以及OBS的桶名bucket_name和终端节点endpoint

```python
obs_ak = "*****" 							# Access Key
obs_sk = "*****"						 	# Secret Key
obs_endpoint = "obs.XXX.myhuaweicloud.com"  # OBS终端节点地址
obs_bucket_name = "ploto-test"				# OBS桶名
```



#### 2.2 运行服务

- 数据库迁移


切换到**manage.py**所在的目录下，通过终端启动数据库迁移服务

```
python3 manage.py makemigrations
python3 manage.py migrate
```

- 创建超级用户，填入用户名与密码

```
python3 manage.py createsuperuser
```

- 运行服务，指定运行在0.0.0.0，端口为8000，

```
python3 manage.py runserver 0.0.0.0:8000
```

- 创建账户与登录

  本地访问 http://0.0.0.0:8000/admin/auth/user/，若在服务器外访问则使用服务器地址，使用刚刚创建的超级用户登录Django后台

- 创建用户**ploto_test**，在用户权限中给予相应权限；

- 增加Ploto users，选择新建的用户ploto_test，保存后该用户即被激活（不进行此操作无法登录）,并给用户配置相应权限

- 通过 http://0.0.0.0:8000/ 访问系统页面，或者通过服务器地址进行访问
  
- 创建ES索引
  
  ```
  python3 manage.py serach_index --rebuild
  ```
  
- 启动Celery的异步任务

  ```
  celery -A ploto worker -l info -P eventlet
  ```
  
- 启动Celery定时任务

  ```
  celery -A ploto beat
  ```

- 可以运行initutils工具类中的脚本对平台数据进行填充、删除

  fill_obs.py：对obs的填充测试数据

  fill_db.py :   对数据库进行填充测试数据

  delete_test_data.py：删除测试数据



## 加密信息配置

如果需要对密码明文的敏感信息进行加密，可参考本小结实现加密。

对密码明文信息进行加密，并实现密钥密文分离，使用AEC_GCM加密算法，以数据库密码加密为例：

- 加密算法于**ploto/common/common_crypte.py**
- 密钥存放于**ploto/conf/conf.py**
- 密文存放于**ploto/ploto/dev.py**



### 生成密钥与密文

- 生成加密密钥**aes_gcm_key** 
  AES GCM加密算法的秘钥配置，秘钥长度32字节，填写密钥ascii码的base64格式字符串。可以使用以下代码生成加密密钥，把获取的**aes_gcm_key**填写到ploto/conf/conf.py中。**建议周期性替换秘钥，修改密码。**

    ```python
  import binascii
  from Crypto import Random
  aes_gcm_key = binascii.b2a_base64(Random.get_random_bytes(32))
    ```

- 生成密文信息

  在ploto/common/common_crypt.py中，填入“密码明文”，使用之前生成的**aes_gcm_key**进行加密，输出加密后的密文信息（bytes格式），**使用后删除明文信息**。

  ```python
  from conf.conf import aes_gcm_key 		# 加密密钥
  mysql_pwd, mysql_aes_gcm_tag, mysql_aes_gcm_iv = encrypt_aes256gcm("密码明文", key=aes_gcm_key, tag_len=16)
  ```

  输出:

  ```
  crypt_text： b'*****' #加密密文
  tag： b'*****'		 #密文校验信息
  iv： b'*****'		 #随机初始向量	
  ```

  

### **数据库配置** 

  在**ploto/ploto/dev.py**中数据库连接配置可参考以下方式

  ```python
import sys
sys.path.append("..")
from common import common_crypte
mysql_pwd: 	b"*****"						 # 密码密文
mysql_aes_gcm_tag：b"*****"					# 密文校验信息
mysql_aes_gcm_iv： b"*****"					# 随机初始向量

# 数据库配置
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "ploto_test",
        "USER": "root",
        # 调用解密函数获得密码
        "PASSWORD": common_crypte.decrypt_aes256gcm(mysql_pwd, aes_gcm_key, 																mysql_aes_gcm_iv, mysql_aes_gcm_tag),
        "HOST": "127.0.0.1",
        "PORT": "3306",
    },
  ```

  
