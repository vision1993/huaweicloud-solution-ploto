from __future__ import absolute_import
from django.db import models
from django.contrib.auth.models import User

DataType = (
    (0, u'融合数据'),
    (1, u'CAN/GPS'),
    (2, u'相机'),
    (3, u'毫米波雷达'),
    (4, u'激光雷达'),
    (5, u'超声波'),
)

StorageType = (
    (0, u'标准存储'),
    (1, u'低频存储'),
    (2, u'归档存储'),
)

DataStatus = (
    (0, u'待审核'),
    (1, u'标注中'),
    (2, u'标注完成'),
)

RestorationStatus = (
    (0, u'--'),
    (1, u'未恢复'),
    (2, u'恢复中'),
    (3, u'已恢复'),
)


# Create your models here.


class OriginalData(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name="数据包名称", max_length=100, default="")
    size = models.IntegerField(verbose_name="数据包大小(bytes)", default=0)
    bucket_name = models.CharField(verbose_name="桶名", max_length=1024, default="")
    object_key = models.CharField(verbose_name="对象名", max_length=1024, default="")
    storage_type = models.SmallIntegerField(verbose_name="存储类型", choices=StorageType)
    restoration_status = models.SmallIntegerField(verbose_name="恢复状态", choices=RestorationStatus, default=0)
    collect_time = models.DateTimeField(verbose_name="采集时间")
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    is_delete = models.BooleanField(verbose_name="是否删除", default=False)


class AnonymizedData(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name="数据包名称", max_length=100, default="")
    size = models.IntegerField(verbose_name="数据包大小(bytes)", default=0)
    car_id = models.CharField(verbose_name="车辆编号", max_length=50, default="")
    data_type = models.SmallIntegerField(verbose_name="数据类型", choices=DataType)
    storage_type = models.SmallIntegerField(verbose_name="存储类型", choices=StorageType)
    restoration_status = models.SmallIntegerField(verbose_name="恢复状态", choices=RestorationStatus, default=0)
    collect_time = models.DateTimeField(verbose_name="采集时间")
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    tag = models.CharField(verbose_name="数据标签", max_length=1024, default="")
    region = models.CharField(verbose_name="采集地区", max_length=50, default="")
    remark = models.CharField(verbose_name="描述", max_length=1024, default="")
    url = models.CharField(verbose_name="存储路径", max_length=512, default="")
    bucket_name = models.CharField(verbose_name="桶名", max_length=1024, default="")
    object_key = models.CharField(verbose_name="对象名", max_length=1024, default="")
    version = models.CharField(verbose_name="数据版本", max_length=100, default="")
    user_safety = models.CharField(verbose_name="安全专员", max_length=20, default="")
    user_collect = models.CharField(verbose_name="采集专员", max_length=20, default="")
    download_times = models.IntegerField(verbose_name="下载次数", default=0)
    is_delete = models.BooleanField(verbose_name="是否删除", default=False)
    source_type = models.SmallIntegerField(verbose_name="数据来源类型", default=0)
    source_id = models.IntegerField(verbose_name="数据关联ID", default=None)
    uuid = models.CharField(verbose_name="uuid", max_length=1024, default="")

    class Meta:
        ordering = ['-collect_time']
        permissions = (
            ("download_anonymizeddata", "Can download anonymize data"),
            ("restore_anonymizeddata", "Can restore anonymize data"),
            ("modify_class_anonymizeddata", "Can modify storage class anonymize data"),
        )
        unique_together = ['url']


class SceneData(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(verbose_name="数据包名称", max_length=100, default="")
    size = models.IntegerField(verbose_name="数据包大小(bytes)", default=0)
    car_id = models.CharField(verbose_name="车辆编号", max_length=50, default="")
    data_status = models.SmallIntegerField(verbose_name="数据状态", choices=DataStatus)
    data_type = models.SmallIntegerField(verbose_name="数据类型", choices=DataType)
    storage_type = models.SmallIntegerField(verbose_name="存储类型", choices=StorageType)
    restoration_status = models.SmallIntegerField(verbose_name="恢复状态", choices=RestorationStatus, default=0)
    scenario_id = models.CharField(verbose_name="场景类型", max_length=100, default="overtake")
    collect_time = models.DateTimeField(verbose_name="采集时间")
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    tag = models.CharField(verbose_name="数据标签", max_length=1024, default="")
    region = models.CharField(verbose_name="采集地区", max_length=50, default="")
    remark = models.CharField(verbose_name="描述", max_length=1024, default="")
    url = models.CharField(verbose_name="存储路径", max_length=512, default="")
    bucket_name = models.CharField(verbose_name="桶名", max_length=1024, default="")
    object_key = models.CharField(verbose_name="对象名", max_length=1024, default="")
    version = models.CharField(verbose_name="数据版本", max_length=100, default="")
    user_safety = models.CharField(verbose_name="安全专员", max_length=20, default="")
    user_collect = models.CharField(verbose_name="采集专员", max_length=20, default="")
    user_review = models.CharField(verbose_name="审核人员", max_length=20, default="")
    duration = models.IntegerField(verbose_name="时长(秒)", default=3)
    download_times = models.IntegerField(verbose_name="下载次数", default=0)
    from_data_id = models.IntegerField(verbose_name="来源脱敏id", default=1)
    is_delete = models.BooleanField(verbose_name="是否删除", default=False)
    uuid = models.CharField(verbose_name="uuid", max_length=1024, default="")

    class Meta:
        ordering = ['-collect_time']
        permissions = (
            ("download_scenedata", "Can download scene data"),
            ("restore_scenedata", "Can restore scene data"),
            ("modify_class_scenedata", "Can modify storage class scene data"),
        )
        unique_together = ['url']


class DataDownloadInfo(models.Model):
    data_from = (
        (0, u'原始数据'),
        (1, u'脱敏数据'),
        (2, u'场景片段'),
    )
    id = models.AutoField(primary_key=True)
    data_id = models.IntegerField(verbose_name="数据源ID")
    data_type = models.SmallIntegerField(verbose_name="数据类型", choices=data_from)
    data_version = models.CharField(verbose_name="数据版本", max_length=100, default="")
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="data_download_infos")
    time = models.DateTimeField(verbose_name="下载时间", auto_now_add=True)


class AnnotateData(models.Model):
    id = models.AutoField(primary_key=True)
    bucket_name = models.CharField(verbose_name="桶名", max_length=1024, default="")
    object_key = models.CharField(verbose_name="对象名", max_length=1024, default="")
    is_delete = models.BooleanField(verbose_name="是否删除", default=False)


class ObsAsyncJobStatus(models.Model):
    job_type_choices = (
        (0, u'正常取回'),
        (1, u'加急取回'),
    )
    job_data_choices = (
        (0, u'脱敏数据'),
        (1, u'场景数据'),
    )
    id = models.AutoField(primary_key=True)
    job_id = models.CharField(verbose_name="任务ID", max_length=100, default="")
    job_type = models.SmallIntegerField(verbose_name="取回方式", choices=job_type_choices)
    job_data_type = models.SmallIntegerField(verbose_name="数据类型", choices=job_data_choices)
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    job_bucket = models.CharField(verbose_name="桶名", max_length=100, default="")
    job_object = models.CharField(verbose_name="对象名", max_length=512, default="")
    next_run_time = models.DateTimeField(verbose_name="下一次执行时间")


class SceneDataCount(models.Model):
    main_scene = models.CharField(verbose_name="主场景类型", max_length=100, default="")
    scenario_id = models.CharField(verbose_name="细分场景类型", max_length=100, default="")
    size = models.IntegerField(verbose_name="数据包大小(bytes)", default=0)
    scenario_number = models.IntegerField(verbose_name="场景数量", default=0)


class AsyncJobAnonymizedAdd(models.Model):
    id = models.AutoField(primary_key=True)
    anonymized_id = models.ForeignKey(AnonymizedData, on_delete=models.CASCADE, related_name="async_job_anonymized_add",
                                      db_column='anonymized_id')
    data_id = models.IntegerField(verbose_name="isv平台数据id")
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    task_id = models.CharField(verbose_name="任务ID", max_length=100)
    sessionid = models.CharField(verbose_name="会话ID", max_length=100)
    username = models.CharField(verbose_name="用户名", max_length=100)
    token = models.CharField(verbose_name="token", max_length=255)


class AsyncJobSceneCut(models.Model):
    id = models.AutoField(primary_key=True)
    anonymized_id = models.ForeignKey(AnonymizedData, on_delete=models.CASCADE, related_name="async_job_scene_cut",
                                      db_column='anonymized_id')
    task_id = models.CharField(verbose_name="任务ID", max_length=100)
    data_id = models.IntegerField(verbose_name="isv平台数据id")
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    sessionid = models.CharField(verbose_name="会话ID", max_length=100, blank=True)
    username = models.CharField(verbose_name="用户名", max_length=100, blank=True)
    token = models.CharField(verbose_name="token", max_length=255, blank=True)
    uuid = models.CharField(verbose_name="uuid", max_length=1024, default="")
