"""ploto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from __future__ import absolute_import
from django.urls import path
from data_mgt.views import original_data_views
from data_mgt.views import anonymized_data_views
from data_mgt.views import scene_data_views

urlpatterns = [
    path(r'original', original_data_views.OriginalDataHandler.as_view(), name="OriginalDataAdd"),
    path(r'original/list', original_data_views.BatchOriginalData.as_view(), name="OriginalDataList"),
    path(r'original/<int:pk>', original_data_views.OriginalDataHandler.as_view(), name="OriginalData"),
    path(r'original/download/<int:pk>', original_data_views.OriginalDataFileHandler.as_view(),
         name="OriginalDataDownload"),
    path(r'anonymized', anonymized_data_views.AnonymizedDataHandler.as_view(), name="AnonymizedDataAdd"),
    path(r'anonymized/list', anonymized_data_views.BatchAnonymizedData.as_view(), name="AnonymizedDataList"),
    path(r'anonymized/<int:pk>', anonymized_data_views.AnonymizedDataHandler.as_view(), name="AnonymizedData"),
    path(r'anonymized/download/<int:pk>', anonymized_data_views.AnonymizedDataFileHandler.as_view(),
         name="AnonymizedData"),
    path(r'anonymized/restore', anonymized_data_views.AnonymizedDataRestore.as_view(), name="AnonymizedDataRestore"),
    path(r'anonymized/storageclass', anonymized_data_views.AnonymizedDataModifyStorageClass.as_view(),
         name="AnonymizedDataModifyStorageClass"),
    path(r'anonymized/scenecut', anonymized_data_views.AnonymizedDataCirculate.as_view(),
         name='AnonymizedDataCirculate'),
    path(r'scene', scene_data_views.SceneDataHandler.as_view(), name="SceneDataAdd"),
    path(r'scene/list', scene_data_views.BatchSceneData.as_view(), name="SceneDataList"),
    path(r'scene/<int:pk>', scene_data_views.SceneDataHandler.as_view(), name="SceneData"),
    path(r'scene/download/<int:pk>', scene_data_views.SceneDataFileHandler.as_view(), name="SceneDataDownload"),
    path(r'scene/restore', scene_data_views.SceneDataRestore.as_view(), name="SceneDataRestore"),
    path(r'scene/storageclass', scene_data_views.SceneDataModifyStorageClass.as_view(),
         name="SceneDataModifyStorageClass"),
]
