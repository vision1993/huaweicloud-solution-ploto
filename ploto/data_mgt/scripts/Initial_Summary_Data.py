from __future__ import absolute_import

from django.db.models import Sum, Count
from data_mgt.models import SceneData, SceneDataCount

from common.tools import scenario_conversion_main_scene

def initial_summary_data():
    list_scenario_id = SceneData.objects.filter(is_delete=0).values('scenario_id').annotate(
        subcategory_size=Sum('size'),
        subcategory_count=Count('size'))
    for scenario in list_scenario_id:
        main_scene = scenario_conversion_main_scene(scenario['scenario_id'])
        SceneDataCount.objects.create(main_scene=main_scene, scenario_id=scenario['scenario_id'],
                                      size=scenario['subcategory_size'], scenario_number=scenario['subcategory_count'])


def run():
    initial_summary_data()
