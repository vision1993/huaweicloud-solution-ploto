from __future__ import absolute_import
from rest_framework import serializers

from data_mgt.models import OriginalData, AnonymizedData, SceneData, DataDownloadInfo, AnnotateData


class OriginalDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = OriginalData
        fields = '__all__'


class AnonymizedDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnonymizedData
        fields = '__all__'


class SceneDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = SceneData
        fields = '__all__'


class AnnotateDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnotateData
        fields = '__all__'


class DataDownloadInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataDownloadInfo
        fields = '__all__'
