"""ploto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from __future__ import absolute_import
from django.urls import path

from monitor import views

urlpatterns = [
    path(r'check', views.AdministratorCheck.as_view(), name="AdministratorCheck"),
    path(r'summarize', views.TotalDataDisplay.as_view(), name="TotalDataDisplay"),
    path(r'original', views.OriginalDataDisplay.as_view(), name="OriginalDataDisplay"),
    path(r'annotate', views.AnnotateDataDisplay.as_view(), name="AnnotateDataDisplay"),
    path(r'annotate/details', views.AnnotateDataDetails.as_view(), name="AnnotateDataDetails"),
    path(r'train', views.TrainDataDisplay.as_view(), name="TrainDataDisplay"),
    path(r'train/details', views.TrainTaskDetails.as_view(), name="TrainTaskDetails"),
    path(r'emulation', views.EmulationDataDisplay.as_view(), name="EmulationDataDisplay"),
    path(r'emulation/details', views.EmulationDataDetails.as_view(), name="EmulationDataDetails"),
    path(r'scene', views.SceneDataDisplay.as_view(), name="SceneDataDisplay"),
    path(r'scene/details', views.SceneDataDetails.as_view(), name="SceneDataDetails"),
    path(r'alarm', views.MonitorAlarmDisplay.as_view(), name="MonitorAlarmDisplay"),
    path(r'news', views.MonitorNewsDisplay.as_view(), name="MonitorNewsDisplay"),
]
