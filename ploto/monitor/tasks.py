from __future__ import absolute_import
import logging
from django.utils import timezone

from monitor.models import MonitorRealTimeInfo
from ploto.celery import app as celery_app
#
#
logger = logging.getLogger(__name__)


@celery_app.task
def timing_delete_realtime_info():
    cur_date = timezone.now().date()
    yesterday = cur_date - timezone.timedelta(days=1)
    MonitorRealTimeInfo.objects.filter(create_time__lt=yesterday).update(is_delete=True)
