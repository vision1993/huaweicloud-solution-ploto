from __future__ import absolute_import
from django.db import models


TrainType = (
    (0, u'感知融合'),
    (1, u'前置中摄'),
    (2, u'后置中摄'),
)


AnnotateData = (
    (0, u'图片'),
    (1, u'点云'),
)


class TotalDataMonitor(models.Model):
    total_size = models.IntegerField(verbose_name="数据量大小(TB)", default=0)
    duration = models.IntegerField(verbose_name="采集时长(时)", default=0)
    mileage = models.IntegerField(verbose_name="里程数(KM)", default=0)
    car_count = models.IntegerField(verbose_name="采集车数量(辆)", default=0)
    car_in_transit = models.IntegerField(verbose_name="在途车辆(辆)", default=0)

    class Meta:
        permissions = (
            ("Admin_TotalData", "Can manage all monitor data"),
        )


class OriginalDataMonitor(models.Model):
    area = models.CharField(verbose_name="城市", max_length=50, default="")
    graph_business = models.CharField(verbose_name="图商", max_length=50, default="")
    size = models.IntegerField(verbose_name="数据量大小(TB)", default=0)
    duration = models.IntegerField(verbose_name="时长(时)", default=0)
    mileage = models.IntegerField(verbose_name="里程数(KM)", default=0)


class AnnotateDataMonitor(models.Model):
    automatic_tasks = models.IntegerField(verbose_name="自动标注数", default=0)
    add_tasks = models.IntegerField(verbose_name="今日新增任务", default=0)
    pending_tasks = models.IntegerField(verbose_name="待自动标注数", default=0)
    expect_annotate_time = models.IntegerField(verbose_name="预计自动标注时长（分）", default=0)


class AnnotateVendorDetails(models.Model):
    annotate_vendor = models.CharField(verbose_name="标注厂商", max_length=50, default="")
    annotate_data = models.IntegerField(verbose_name="标注数据类型", choices=AnnotateData)
    annotate_number = models.IntegerField(verbose_name="标注数量", default=0)
    complete_time = models.DateTimeField(verbose_name="完成时间", auto_now_add=True)


class TrainDataMonitor(models.Model):
    train_tasks = models.IntegerField(verbose_name="训练任务", default=0)
    pending_tasks = models.IntegerField(verbose_name="待训练任务", default=0)
    gpu_consumes = models.IntegerField(verbose_name="GPU消耗", default=0)
    average_train_duration = models.IntegerField(verbose_name="训练时长(秒)", default=0)


class TrainDataDetails(models.Model):
    name = models.CharField(verbose_name="训练算法名称", max_length=100, default="")
    train_type = models.SmallIntegerField(verbose_name="训练算法", choices=TrainType)
    train_version = models.CharField(verbose_name="版本信息", max_length=50, default="")
    train_times = models.IntegerField(verbose_name="训练次数", default=0)
    train_precision = models.IntegerField(verbose_name="训练精度", default=0)
    train_progress = models.IntegerField(verbose_name="训练进度", default=0)


class EmulationDataMonitor(models.Model):
    test_tasks = models.IntegerField(verbose_name="测试任务数", default=0)
    pending_tasks = models.IntegerField(verbose_name="待测试任务数", default=0)
    all_test_mileage = models.IntegerField(verbose_name="总测试里程(KM)", default=0)
    week_test_mileage = models.IntegerField(verbose_name="本周测试里程(KM)", default=0)
    success_rate = models.FloatField(verbose_name="任务成功率（%）", default=0)
    success_rate_upscale = models.FloatField(verbose_name="成功率提升百分比（%）", default=0)


class EmulationDetails(models.Model):
    region = models.CharField(verbose_name="城市", max_length=50, default="")
    main_scene = models.CharField(verbose_name="主仿真场景", max_length=50, default="")
    segmentation_scene = models.CharField(verbose_name="细分场景", max_length=50, default="")
    completed_tasks = models.IntegerField(verbose_name="已完成任务", default=0)
    complete_time = models.DateTimeField(verbose_name="完成时间", auto_now_add=True)


class SceneDataMonitor(models.Model):
    region = models.CharField(verbose_name="城市", max_length=50, default="")
    size = models.IntegerField(verbose_name="数据包大小(bytes)", default=0)
    main_scene = models.CharField(verbose_name="主场景类型", max_length=100, default="")
    scenario_id = models.CharField(verbose_name="细分场景类型", max_length=100, default="")
    scenario_number = models.IntegerField(verbose_name="场景数量", default=0)
    bag_id = models.IntegerField(verbose_name="场景bag_id", default=0)
    complete_time = models.DateTimeField(verbose_name="完成时间", auto_now_add=True)


class MonitorAlarm(models.Model):
    AlarmLevel = (
        (0, u'紧急'),
        (1, u'严重'),
        (2, u'一般'),
    )
    id = models.AutoField(primary_key=True)
    alarm_time = models.DateTimeField(verbose_name="告警发生时间")
    alarm_content = models.CharField(verbose_name="告警内容", max_length=255, default="")
    alarm_details = models.CharField(verbose_name="消息详细内容", max_length=255, default="")
    level = models.SmallIntegerField(verbose_name="告警级别", choices=AlarmLevel)
    is_delete = models.BooleanField(verbose_name="是否清除", default=False)

    class Meta:
        ordering = ['-alarm_time']


class MonitorRealTimeInfo(models.Model):
    id = models.AutoField(primary_key=True)
    region = models.CharField(verbose_name="城市", max_length=50, default="")
    create_time = models.DateTimeField(verbose_name="新消息产生时间")
    info_content = models.CharField(verbose_name="消息内容", max_length=100, default="")
    is_delete = models.BooleanField(verbose_name="是否清除", default=False)

    class Meta:
        ordering = ['-create_time']
