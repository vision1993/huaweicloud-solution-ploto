from __future__ import absolute_import
from rest_framework import serializers

from monitor.models import TotalDataMonitor, OriginalDataMonitor, AnnotateDataMonitor, \
    TrainDataMonitor, EmulationDataMonitor, \
    SceneDataMonitor, MonitorAlarm


class TotalDataMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = TotalDataMonitor
        fields = '__all__'


class OriginalDataMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = OriginalDataMonitor
        fields = '__all__'


class AnnotateDataMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnotateDataMonitor
        fields = '__all__'


class TrainDataMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainDataMonitor
        fields = '__all__'


class EmulationDataMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmulationDataMonitor
        fields = '__all__'


class SceneDataMonitorSerializer(serializers.ModelSerializer):
    class Meta:
        model = SceneDataMonitor
        fields = '__all__'


class MonitorAlarmSerializer(serializers.ModelSerializer):
    class Meta:
        model = MonitorAlarm
        fields = '__all__'
