"""ploto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from __future__ import absolute_import
from django.urls import path

from user import views

urlpatterns = [
    path(r'login', views.UserLogin.as_view(), name="UserLogin"),
    path(r'ADlogin', views.ADUserLogin.as_view(), name="ADUserLogin"),
    path(r'logout', views.UserLogout.as_view(), name="UserLogout"),
]
