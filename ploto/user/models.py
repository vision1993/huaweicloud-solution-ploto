from __future__ import absolute_import
from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class PlotoUser(models.Model):
    user_type_choice = (
        (0, "普通账号"),
        (1, "AD域账号"),
    )
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="ploto_user")
    phone = models.CharField(verbose_name="电话", max_length=20, blank=True)
    dept = models.CharField(verbose_name="部门", max_length=120, default="")
    type = models.SmallIntegerField(verbose_name="账号类型", choices=user_type_choice, default=0)

    def __str__(self):
        return self.user.username

    def name(self):
        return self.user.username
    name.admin_order_field = 'user'
    name.short_description = "账户"

    def employee_name(self):
        return self.user.first_name + " " + self.user.last_name
    employee_name.short_description = "姓名"

    def user_email(self):
        return self.user.email
    user_email.short_description = "邮箱"

    def user_is_active(self):
        return self.user.is_active
    user_is_active.short_description = "是否激活"
    user_is_active.boolean = True
