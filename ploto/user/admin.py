from __future__ import absolute_import
from django.contrib import admin
from user.models import PlotoUser


class PlotoUserAdmin(admin.ModelAdmin):
    list_display = ('name', 'employee_name', 'dept', 'phone', 'user_email', 'user_is_active')
    list_filter = ['dept', 'user']
    search_fields = ['user__username', 'dept', 'phone', 'user__email']
    search_help_text = "可按照账户、部门、邮箱、电话进行模糊搜索"


# Register your models here.
admin.site.register(PlotoUser, PlotoUserAdmin)
