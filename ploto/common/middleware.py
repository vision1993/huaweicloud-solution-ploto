from __future__ import absolute_import

import logging
from django.urls import resolve
from django.utils.deprecation import MiddlewareMixin
from django.conf import settings

logger = logging.getLogger(__name__)


class DisableCSRF(MiddlewareMixin):

    def process_request(self, request):
        # 登录登出跳过CSRFToken校验
        if resolve(request.path_info).view_name in settings.DISABLE_CSRF_VIEWS:
            setattr(request, '_dont_enforce_csrf_checks', True)

        return
