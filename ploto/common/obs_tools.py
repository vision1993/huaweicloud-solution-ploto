from __future__ import absolute_import

import os
from pathlib import Path

from obs import ObsClient, LogConf, RestoreTier
from conf.conf import obs_ak, obs_endpoint, obs_sk

obs_client = ObsClient(access_key_id=obs_ak,
                       secret_access_key=obs_sk, server=obs_endpoint)

ploto_dir = Path(__file__).resolve().parent.parent
obs_log_file = os.path.join(ploto_dir, "conf", "obs_log.conf")
obs_client.initLog(LogConf(obs_log_file), "PLOTO_OBS_LOGGER")


def object_restore_status(bucket_name, object_key):
    restore_status = ''
    res = obs_client.getObjectMetadata(bucket_name, object_key)
    restore_out = res.body.restore
    if restore_out is None:
        restore_status = 'no_restored'  # 归档存储，未恢复
    elif restore_out.find("false") != -1:
        restore_status = 'restored'  # 归档存储，已恢复
    elif restore_out.find("true") != -1:
        restore_status = 'restoring'  # 归档存储，恢复中
    return restore_status


def get_job_type(tier):
    if tier == RestoreTier.EXPEDITED:
        return 1
    else:
        return 0
