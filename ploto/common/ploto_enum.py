from __future__ import absolute_import

from enum import Enum

class BaseEnum(Enum):

    @property
    def code(self):
        return self.value[0]

    @property
    def msg(self):
        return self.value[1]


class CommonStatuEnum(BaseEnum):
    OK = ("HSP.10000", "请求成功")
    VALID_EXCEPTION = ("HSP.10001", "请求参数非法，缺少必要参数或者参数格式不正确")
    IDENTIFICATION_EXCEPTION = ("HSP.10002", "身份认证失败")
    AUTHORIZATION_EXCEPTION = ("HSP.10003", "用户未授权")
    PROTOCOL_EXCEPTION = ("HSP.10004", "不支持当前协议")
    INTERNAL_ERROR = ("HSP.10005", "服务器内部出现错误")


class DataStatuEnum(BaseEnum):
    ANONYMIZE_URL_EXCEPTION = ("HSP.11001", "数据url格式不正确")
    DOWNLOAD_EXCEPTION = ("HSP.11002", "未恢复以及恢复中的对象不允许下载")
    SET_METADATA_EXCEPTION = ("HSP.11003", "设置数据对象元数据失败")
    GET_DOWNLOAD_URL_EXCEPTION = ("HSP.11004", "获取下载url失败")
    RESTORE_EXCEPTIOM = ("HSP.11005", "恢复对象或修改已恢复对象失效时间失败")
    RESTORING_NOT_ALLOWED = ("HSP.11006", "恢复中对象不允许再次请求恢复")
    NOT_EXIST_EXCEPTION = ("HSP.11007", "所请求数据对象不存在")
    MODIFY_CLASS_EXCEPTION = ("HSP.11008", "修改存储类别失败")
    MODIFY_CLASS_NOT_ALLOWED = ("HSP.11009", "未恢复或恢复中的归档存储对象不允许修改存储类别")
    GET_SCENE_LIST_EXCEPTION = ("HSP.11010", "获取数据列表失败")
    ALARM_STATUS_EXCEPTION = ("HSP.12001", "目标告警状态非法")
    ALARM_CHANGE_EXCEPTION = ("HSP.12002", "当前告警状态不允许修改为目标状态")
