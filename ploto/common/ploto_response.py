class PlotoResponse(object):

    def __init__(self):
        self.msg = ""
        self.code = ""
        self.data = {}

    @property
    def dict(self):
        if not self.msg:
            self.msg = "请求成功"
        if not self.code:
            self.code = "HSP.10000"
        if self.data:
            return self.__dict__
        else:
            resp = {"msg": self.msg, "code": self.code}
            return resp
