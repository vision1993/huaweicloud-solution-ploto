# 加密秘钥，base64字符串格式
aes_gcm_key = '**********************'

# mysql数据库登录配置
mysql_user = "root"
mysql_pwd = "******************"
mysql_aes_gcm_iv = "******************"
mysql_aes_gcm_tag = "******************"

obs_ak = "xxxx"
obs_sk = "xxxxxxxx"
obs_endpoint = "obs.cn-south-1.myhuaweicloud.com"

data_mgt_apis_domain = "https://data.holomatic.com"
data_mgt_apis = {
    "bags_add": data_mgt_apis_domain + "/api/bags/add",
    "bags_detail": data_mgt_apis_domain + "/api/bags/detail",
    "bags_list": data_mgt_apis_domain + "/api/bags/search",
    "scenario_bag_detail": data_mgt_apis_domain + "/api/scenario_bag/detail",
    "scenario_bag_list": data_mgt_apis_domain + "/api/scenario_bag/search",
    "scene_cut": data_mgt_apis_domain + "/api/task/cut_scene",
    "scene_cut_task_detail": data_mgt_apis_domain + "/api/task/detail",
    "scene_cut_task_cancel": data_mgt_apis_domain + "/api/task/cancel",
    "scene_cut_task_log": data_mgt_apis_domain + "/api/task/log"
}

obs_bucket_name = "*******"
anonymized_data_url = "http://localhost:8000/data/anonymized"
scene_data_url = "http://localhost:8000/data/scene"

ces_ak = obs_ak
ces_sk = obs_sk
ces_endpoint = "*****************************"
ces_project_id = "****************************"
