from __future__ import absolute_import

from django.db import models


# Create your models here.
class EventData(models.Model):
    event_type = models.CharField(verbose_name="事件类型", max_length=50, default="")
    event_name = models.CharField(verbose_name="事件名称", max_length=100, default="")
    event_source = models.CharField(verbose_name="事件来源", max_length=50, default="")
    resource_type = models.CharField(verbose_name="资源类型", max_length=50, default="")
    resource_id = models.TextField(verbose_name="资源ID", max_length=2000, default="")
    resource_name = models.TextField(verbose_name="资源名称", max_length=2000, default="")
    state = models.CharField(verbose_name="操作状态", max_length=50, default="")
    level = models.CharField(verbose_name="事件级别", max_length=50, default="")
    username = models.CharField(verbose_name="操作用户", max_length=50, default="")
    create_time = models.DateTimeField(verbose_name="操作时间", auto_now_add=True)
    details = models.JSONField(verbose_name="详情", default=dict)

    class Meta:

        ordering = ['-create_time']
        default_permissions = ("add", "change", "delete", "view")


class AlarmData(models.Model):
    alarm_type = models.CharField(verbose_name="告警类型", max_length=50, default="")
    alarm_name = models.CharField(verbose_name="告警名称", max_length=100, default="")
    alarm_source = models.CharField(verbose_name="告警来源", max_length=50, default="")
    state = models.CharField(verbose_name="告警状态", max_length=50, default="")
    level = models.CharField(verbose_name="告警级别", max_length=50, default="")
    last_operate_time = models.DateTimeField(verbose_name="最后操作时间", max_length=50, auto_now_add=True)
    details = models.JSONField(verbose_name="详情", default=dict)
    class Meta:

        ordering = ['-last_operate_time']
        default_permissions = ("add", "change", "delete", "view")
        