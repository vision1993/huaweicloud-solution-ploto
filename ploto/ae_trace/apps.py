from __future__ import absolute_import
import os
from django.apps import AppConfig

class TraceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ae_trace'

    template_event = os.path.abspath(
        os.path.join(__file__, '..', 'resource', 'template_event.yml'))
    template_alarm = os.path.abspath(
        os.path.join(__file__, '..', 'resource', 'template_alarm.yml'))

