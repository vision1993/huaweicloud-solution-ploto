ALLOWED_HOSTS = ["*"]
SESSION_COOKIE_SAMESITE = None
CSRF_COOKIE_SAMESITE = None
DEBUG = True
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "ploto_test",
        "USER": "root",
        "PASSWORD": "********",
        "HOST": "127.0.0.1",
        "PORT": "3306",
    },
    'slave': {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "ploto_test",
        "USER": "root",
        "PASSWORD": "********",
        "HOST": "127.0.0.2",
        "PORT": "3306",
    }
}
ES_HOST = '127.0.0.1'
ES_PORT = '9200'
ES_USERNAME = 'username'
ES_PASSWORD = 'password'

ELASTICSEARCH_DSL={
    'default':{
        'hosts': f'https://{ES_HOST}:{ES_PORT}',
        'http_auth':f'{ES_USERNAME}:{ES_PASSWORD}',
        'verify_certs': False
    }
}

REDIS_HOST = '127.0.0.1'
REDIS_PORT = '6379'
REDIS_PASSWORD = '********'

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://" + REDIS_HOST + ":" +
                    REDIS_PORT + "/9",
        "TIMEOUT": 3600 * 60,  # 缓存的有效时间(s)
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {
                "max_connections": 100
            },
            "PASSWORD": REDIS_PASSWORD,
        }
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"
SESSION_COOKIE_AGE = 60 * 60  # session的有效时间(s)
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_SAVE_EVERY_REQUEST = True

BROKER_URL = 'redis://:' + REDIS_PASSWORD + \
             '@' + REDIS_HOST + ':' + REDIS_PORT + '/0'
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600}
CELERY_RESULT_BACKEND = 'redis://:' + REDIS_PASSWORD + \
                        '@' + REDIS_HOST + ':' + REDIS_PORT + '/0'
CELERY_RESULT_BACKEND = 'django-db'
