from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ploto.settings')

app = Celery('ploto')

app.config_from_object('django.conf:settings')

# load task modules from all registered Django app configs
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    'get_obs_job_status': {
        'task': 'data_mgt.tasks.get_obs_job_status',
        'schedule': crontab(minute="*/6")
    },
    'timing_delete_realtime_info': {
        'task': 'monitor.tasks.timing_delete_realtime_info',
        'schedule': crontab(minute="*/10")
    },
    'monitor_event_alarm': {
        'task': 'ae_trace.tasks.monitor_event_alarm',
        'schedule': crontab(minute="*/5")
    },
    'monitor_resource_alarm': {
        'task': 'ae_trace.tasks.monitor_resource_alarm',
        'schedule': crontab(minute="*/10")
    },
    'monitor_network_alarm': {
        'task': 'ae_trace.tasks.monitor_network_alarm',
        'schedule': crontab(minute="*/1")
    },
    'async_check_anonymize': {
        'task': 'data_mgt.tasks.async_check_anonymize',
        'schedule': crontab(minute="*/1")
    },
    'async_check_scene_cut': {
        'task': 'data_mgt.tasks.async_check_scene_cut',
        'schedule': crontab(minute="*/1")
    },
}
