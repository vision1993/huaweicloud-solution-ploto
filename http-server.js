var express = require('express')
var compression = require('compression')
var contextPath = require('./vue.config').publicPath
var http = require('http')
var path = require('path')
var app = express()

app.use(compression())
app.use(contextPath, express.static(path.resolve(__dirname, './dist/')))
app.use(contextPath, express.static(path.resolve(__dirname, './web/')))
