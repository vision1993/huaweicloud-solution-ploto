export const searchBoxOptions = [
    {
        id: 'name',
        label: '名称',
        type: 'input',
        options: []
    }, 
    {
        id: 'car_id',
        label: '采集车辆',
        type: 'input',
        options: []
    }, 
    {
        id: 'region',
        label: '区域',
        type: 'input',
        options: []
    }, 
    {
        id: 'tag',
        label: '标签',
        type: 'input',
        options: []
    }, 
    {
        id: 'data_type',
        label: '数据类型',
        type: 'radio',
        options: []
    }, 
    {
        id: 'storage_type',
        label: '存储类型',
        type: 'radio',
        options: []
    }, 
    {
        id: 'collect_time',
        label: '采集时间',
        type: 'dateRange',
        format: 'YYYY/MM/dd',
    }
]