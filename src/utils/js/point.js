import * as echarts from 'echarts';
let fontStyle = {
        fontSize: '10px',
        fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
        fontWeight: 'Normal',
        color: '#A1A1A1'
};
let axisStyle = {
    axisTick: {
        show: false 
    },
    axisLine: {
        show: false 
    },
};
let labelStyle = {
    show: true, 
    position: 'top', 
    textStyle: { 
        ...fontStyle,
        distance: 10 
    }
};
let axisLabelStyle = {
    color: '#131416',
    fontSize: 12,
    show: false
};
let gridStyle = {
    grid: {
        left: 20,
        right: 20
    },
    xAxis: {
        ...axisStyle,
        itemStyle: {
            normal:{ 
                ...fontStyle
            }
        },
        axisLabel:{
            interval: 0,
            //当x轴文字过长时自动换行
            formatter: function(value){
                let ret = ''; 
                let maxLength = 6;
                let rowN = Math.ceil(value.length / maxLength);
                if (rowN > 1){
                   for (let i = 0; i < rowN; i++) {
                        let start = i * maxLength;
                        let end = start + maxLength;
                        ret += value.substring(start, end) + '\n';
                    }
                    return ret;
                }else {
                    return value;
                }
            }
        },
    },
    yAxis: {
        show: false, 
        type: 'value',
    },
};
let yAxisStyle = {
    type: 'value',
    splitLine: {
        show: false
    },
    itemStyle: {
        normal:{ 
            ...fontStyle
        }
    },
};

export const mapOption = {
    backgroundColor: '#0d0d0f',
    tooltip: {
        trigger: 'item',
        alwaysShowContent: true,
        backgroundColor: '#2e3847',
        borderColor: '#ccc',
        borderWidth: 1.1,
        borderRadius: 15,
        position: 'top',
        padding: [10, 30],
        enterable: false,
        textStyle: {
            fontSize: 12,
            color: '#fff' 
        },
        formatter: function (params) {
            var res = params.data.info;
            return res
        },
    },
    geo: {
        type: 'map',
        map: 'china',
        center: [106.114129, 33.550339],
        zoom: 1.3,
        roam: true,
        mapStyle: {
            styleJson: [
                {
                    featureType: 'water',
                    elementType: 'all',
                    stylers: {
                        color: '#0e1014'
                    }
                },
                {
                    featureType: 'land',
                    elementType: 'all',
                    stylers: {
                        color: '#1d2431'
                    }
                },
                {
                    featureType: 'boundary',
                    elementType: 'geometry',
                    stylers: {
                        color: '#3c4c66'
                    }
                },
                {
                    featureType: 'boundary',
                    elementType: 'geometry.fill',
                    stylers: {
                        color: '#3c4c66'
                    }
                }
            ]
        },
        label: {                        
            show: false,                                                                                  
            emphasis: {                               
                show: false,                    
            },                        
        },
        itemStyle: {                          
            areaColor: '#1d2431',                             
            borderWidth: 1.1,                              
            textStyle: {                                    
                color: '#fff'                                
            }, 
            borderColor: '#313c4b',  
        },
        emphasis: {
            itemStyle: {                          
            areaColor: '#1d2431',                             
            borderWidth: 1.1,                                
            textStyle: {                                    
                color: '#fff'                                
            }, 
                borderColor: '#313c4b', 
            },
        },
    },
    series: [
        {
            name: '',
            type: 'effectScatter',
            coordinateSystem: 'geo',
            encode: {
                value: 2
            },
            symbolSize: function (val) {
                return val[2] / 10;
            },
            showEffectOn: 'emphasis',
            rippleEffect: {
                brushType: 'stroke'
            },
            
            label: { 
                formatter: '{b}',
                position: 'bottom',
                color: '#fff',
                fontSize: 14,
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                fontWeight: 'Normal',
                distance: 5,
                show: true,
            },
            itemStyle: {
                color: function(params) {
                    if (params.data.size >= 0 && params.data.size <= params.data.blueValue) {
                        return '#0E4CFF'
                    } else if (params.data.size > params.data.blueValue && params.data.size <= params.data.greenValue) {
                        return '#07AB68'
                    } else if (params.data.size > params.data.greenValue) {
                        return '#FFC617'
                    }
                },
            },
            emphasis: {
                label: {
                    show: true,
                    color: '#fff',
                    position: 'bottom',
                    distance: 5,
                    fontSize: 14
                }
            },
            zlevel: 1
        },
    ],   
};
export const geoCoordMap = { 
    '\u6d77\u95e8': [121.15, 31.89],
    '\u9102\u5c14\u591a\u65af': [109.781327, 39.608266],
    '\u62db\u8fdc': [120.38, 37.35],
    '\u821f\u5c71': [122.207216, 29.985295],
    '\u9f50\u9f50\u54c8\u5c14': [123.97, 47.33],
    '\u76d0\u57ce': [120.13, 33.38],
    '\u8d64\u5cf0': [118.87, 42.28],
    '\u9752\u5c9b': [120.33, 36.07],
    '\u4e73\u5c71': [121.52, 36.89],
    '\u91d1\u660c': [102.188043, 38.520089],
    '\u6cc9\u5dde': [118.58, 24.93],
    '\u83b1\u897f': [120.53, 36.86],
    '\u65e5\u7167': [119.46, 35.42],
    '\u80f6\u5357': [119.97, 35.88],
    '\u5357\u901a': [121.05, 32.08],
    '\u62c9\u8428': [91.11, 29.97],
    '\u4e91\u6d6e': [112.02, 22.93],
    '\u6885\u5dde': [116.1, 24.55],
    '\u6587\u767b': [122.05, 37.2],
    '\u4e0a\u6d77': [121.48, 31.22],
    '\u6500\u679d\u82b1': [101.718637, 26.582347],
    '\u5a01\u6d77': [122.1, 37.5],
    '\u627f\u5fb7': [117.93, 40.97],
    '\u53a6\u95e8': [118.1, 24.46],
    '\u6c55\u5c3e': [115.375279, 22.786211],
    '\u6f6e\u5dde': [116.63, 23.68],
    '\u4e39\u4e1c': [124.37, 40.13],
    '\u592a\u4ed3': [121.1, 31.45],
    '\u66f2\u9756': [103.79, 25.51],
    '\u70df\u53f0': [121.39, 37.52],
    '\u798f\u5dde': [119.3, 26.08],
    '\u74e6\u623f\u5e97': [121.979603, 39.627114],
    '\u5373\u58a8': [120.45, 36.38],
    '\u629a\u987a': [123.97, 41.97],
    '\u7389\u6eaa': [102.52, 24.35],
    '\u5f20\u5bb6\u53e3': [114.87, 40.82],
    '\u9633\u6cc9': [113.57, 37.85],
    '\u83b1\u5dde': [119.942327, 37.177017],
    '\u6e56\u5dde': [120.1, 30.86],
    '\u6c55\u5934': [116.69, 23.39],
    '\u6606\u5c71': [120.95, 31.39],
    '\u5b81\u6ce2': [121.56, 29.86],
    '\u6e5b\u6c5f': [110.359377, 21.270708],
    '\u63ed\u9633': [116.35, 23.55],
    '\u8363\u6210': [122.41, 37.16],
    '\u8fde\u4e91\u6e2f': [119.16, 34.59],
    '\u846b\u82a6\u5c9b': [120.836932, 40.711052],
    '\u5e38\u719f': [120.74, 31.64],
    '\u4e1c\u839e': [113.75, 23.04],
    '\u6cb3\u6e90': [114.68, 23.73],
    '\u6dee\u5b89': [119.15, 33.5],
    '\u6cf0\u5dde': [119.9, 32.49],
    '\u5357\u5b81': [108.33, 22.84],
    '\u8425\u53e3': [122.18, 40.65],
    '\u60e0\u5dde': [114.4, 23.09],
    '\u6c5f\u9634': [120.26, 31.91],
    '\u84ec\u83b1': [120.75, 37.8],
    '\u97f6\u5173': [113.62, 24.84],
    '\u5609\u5cea\u5173': [98.289152, 39.77313],
    '\u5e7f\u5dde': [113.23, 23.16],
    '\u5ef6\u5b89': [109.47, 36.6],
    '\u592a\u539f': [112.53, 37.87],
    '\u6e05\u8fdc': [113.01, 23.7],
    '\u4e2d\u5c71': [113.38, 22.52],
    '\u6606\u660e': [102.73, 25.04],
    '\u5bff\u5149': [118.73, 36.86],
    '\u76d8\u9526': [122.070714, 41.119997],
    '\u957f\u6cbb': [113.08, 36.18],
    '\u6df1\u5733': [114.07, 22.62],
    '\u73e0\u6d77': [113.52, 22.3],
    '\u5bbf\u8fc1': [118.3, 33.96],
    '\u54b8\u9633': [108.72, 34.36],
    '\u94dc\u5ddd': [109.11, 35.09],
    '\u5e73\u5ea6': [119.97, 36.77],
    '\u4f5b\u5c71': [113.11, 23.05],
    '\u6d77\u53e3': [110.35, 20.02],
    '\u6c5f\u95e8': [113.06, 22.61],
    '\u7ae0\u4e18': [117.53, 36.72],
    '\u8087\u5e86': [112.44, 23.05],
    '\u5927\u8fde': [121.62, 38.92],
    '\u4e34\u6c7e': [111.5, 36.08],
    '\u5434\u6c5f': [120.63, 31.16],
    '\u77f3\u5634\u5c71': [106.39, 39.04],
    '\u6c88\u9633': [123.38, 41.8],
    '\u82cf\u5dde': [120.62, 31.32],
    '\u8302\u540d': [110.88, 21.68],
    '\u5609\u5174': [120.76, 30.77],
    '\u957f\u6625': [125.35, 43.88],
    '\u80f6\u5dde': [120.03336, 36.264622],
    '\u94f6\u5ddd': [106.27, 38.47],
    '\u5f20\u5bb6\u6e2f': [120.555821, 31.875428],
    '\u4e09\u95e8\u5ce1': [111.19, 34.76],
    '\u9526\u5dde': [121.15, 41.13],
    '\u5357\u660c': [115.89, 28.68],
    '\u67f3\u5dde': [109.4, 24.33],
    '\u4e09\u4e9a': [109.511909, 18.252847],
    '\u81ea\u8d21': [104.778442, 29.33903],
    '\u5409\u6797': [126.57, 43.87],
    '\u9633\u6c5f': [111.95, 21.85],
    '\u6cf8\u5dde': [105.39, 28.91],
    '\u897f\u5b81': [101.74, 36.56],
    '\u5b9c\u5bbe': [104.56, 29.77],
    '\u547c\u548c\u6d69\u7279': [111.65, 40.82],
    '\u6210\u90fd': [104.06, 30.67],
    '\u5927\u540c': [113.3, 40.12],
    '\u9547\u6c5f': [119.44, 32.2],
    '\u6842\u6797': [110.28, 25.29],
    '\u5f20\u5bb6\u754c': [110.479191, 29.117096],
    '\u5b9c\u5174': [119.82, 31.36],
    '\u5317\u6d77': [109.12, 21.49],
    '\u897f\u5b89': [108.95, 34.27],
    '\u91d1\u575b': [119.56, 31.74],
    '\u4e1c\u8425': [118.49, 37.46],
    '\u7261\u4e39\u6c5f': [129.58, 44.6],
    '\u9075\u4e49': [106.9, 27.7],
    '\u7ecd\u5174': [120.58, 30.01],
    '\u626c\u5dde': [119.42, 32.39],
    '\u5e38\u5dde': [119.95, 31.79],
    '\u6f4d\u574a': [119.1, 36.62],
    '\u91cd\u5e86': [106.54, 29.59],
    '\u53f0\u5dde': [121.420757, 28.656386],
    '\u5357\u4eac': [118.78, 32.04],
    '\u6ee8\u5dde': [118.03, 37.36],
    '\u8d35\u9633': [106.71, 26.57],
    '\u65e0\u9521': [120.29, 31.59],
    '\u672c\u6eaa': [123.73, 41.3],
    '\u514b\u62c9\u739b\u4f9d': [84.77, 45.59],
    '\u6e2d\u5357': [109.5, 34.52],
    '\u9a6c\u978d\u5c71': [118.48, 31.56],
    '\u5b9d\u9e21': [107.15, 34.38],
    '\u7126\u4f5c': [113.21, 35.24],
    '\u53e5\u5bb9': [119.16, 31.95],
    '\u5317\u4eac': [116.46, 39.92],
    '\u5f90\u5dde': [117.2, 34.26],
    '\u8861\u6c34': [115.72, 37.72],
    '\u5305\u5934': [110, 40.58],
    '\u7ef5\u9633': [104.73, 31.48],
    '\u4e4c\u9c81\u6728\u9f50': [87.68, 43.77],
    '\u67a3\u5e84': [117.57, 34.86],
    '\u676d\u5dde': [120.19, 30.26],
    '\u6dc4\u535a': [118.05, 36.78],
    '\u978d\u5c71': [122.85, 41.12],
    '\u6ea7\u9633': [119.48, 31.43],
    '\u5e93\u5c14\u52d2': [86.06, 41.68],
    '\u5b89\u9633': [114.35, 36.1],
    '\u5f00\u5c01': [114.35, 34.79],
    '\u6d4e\u5357': [117, 36.65],
    '\u5fb7\u9633': [104.37, 31.13],
    '\u6e29\u5dde': [120.65, 28.01],
    '\u4e5d\u6c5f': [115.97, 29.71],
    '\u90af\u90f8': [114.47, 36.6],
    '\u4e34\u5b89': [119.72, 30.23],
    '\u5170\u5dde': [103.73, 36.03],
    '\u6ca7\u5dde': [116.83, 38.33],
    '\u4e34\u6c82': [118.35, 35.05],
    '\u5357\u5145': [106.110698, 30.837793],
    '\u5929\u6d25': [117.2, 39.13],
    '\u5bcc\u9633': [119.95, 30.07],
    '\u6cf0\u5b89': [117.13, 36.18],
    '\u8bf8\u66a8': [120.23, 29.71],
    '\u90d1\u5dde': [113.65, 34.76],
    '\u54c8\u5c14\u6ee8': [126.63, 45.75],
    '\u804a\u57ce': [115.97, 36.45],
    '\u829c\u6e56': [118.38, 31.33],
    '\u5510\u5c71': [118.02, 39.63],
    '\u5e73\u9876\u5c71': [113.29, 33.75],
    '\u90a2\u53f0': [114.48, 37.05],
    '\u5fb7\u5dde': [116.29, 37.45],
    '\u6d4e\u5b81': [116.59, 35.38],
    '\u8346\u5dde': [112.239741, 30.335165],
    '\u5b9c\u660c': [111.3, 30.7],
    '\u4e49\u4e4c': [120.06, 29.32],
    '\u4e3d\u6c34': [119.92, 28.45],
    '\u6d1b\u9633': [112.44, 34.7],
    '\u79e6\u7687\u5c9b': [119.57, 39.95],
    '\u682a\u6d32': [113.16, 27.83],
    '\u77f3\u5bb6\u5e84': [114.48, 38.03],
    '\u83b1\u829c': [117.67, 36.19],
    '\u5e38\u5fb7': [111.69, 29.05],
    '\u4fdd\u5b9a': [115.48, 38.85],
    '\u6e58\u6f6d': [112.91, 27.87],
    '\u91d1\u534e': [119.64, 29.12],
    '\u5cb3\u9633': [113.09, 29.37],
    '\u957f\u6c99': [113, 28.21],
    '\u8862\u5dde': [118.88, 28.97],
    '\u5eca\u574a': [116.7, 39.53],
    '\u83cf\u6cfd': [115.480656, 35.23375],
    '\u5408\u80a5': [117.27, 31.86],
    '\u6b66\u6c49': [114.31, 30.52],
    '\u5927\u5e86': [125.03, 46.58]
};
export const anonymizeOverviewOption = {
    grid: {
        left: 0,
        top: 20,
        bottom: 0
    },
    xAxis: {
        show: false,
        type: 'value'
    },
    yAxis: {
        type: 'category',
        inverse: true,
        ...axisStyle,
        axisLabel: axisLabelStyle,
    },
    series: [
        {
            name: '',
            type: 'bar',
            barWidth: 7,
            barCategosryGap: '100%',
            color: '',
            itemStyle: {
                barBorderRadius: 3,
            },
            label: {
                show: true,
                position: [0,-15],
                textStyle: {
                    color: '#A1A1A1',
                    fontSize: 10,
                    fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                    fontWeight: 'Normal',
                    distance: 10
                }
            },
        }
    ]
};
export const anonymizeScreenOption = {
    grid: { 
        left: 20,
        right: 5,
        bottom: 5,
        top: 20
    },
    xAxis: {
        show: false
    },
    yAxis: {
        ...axisStyle,
        axisLabel: axisLabelStyle,
    },
    series: [
        {
            name: '',
            type: 'bar',
            barWidth: 4,
            color: '',
            itemStyle: {
                barBorderRadius: 1
            },
            label: {
                show: true, 
                position: [0,-15], 
                textStyle: { 
                    color: '#fff',
                    fontSize: 12,
                    fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                    fontWeight: 'Normal',
                    distance: 10 
                }
            },
        }
    ]
};
export const labelScreenOption = {
    grid: {
        left: '0%',
        right: '20%',
        bottom: '2%',
        top: '20',
        containLabel: true
    },
    xAxis: {
        show: false
    },
    yAxis: {
        type: 'category',
        axisLabel: {
            color: '#fff',
            fontSize: 12,
            fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
            fontWeight: 'Normal'
        },
        axisLine: {
            show: false
        }
    },
    series: [
        {   
            name: '',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#0E4CFF',
                    barBorderRadius: 1
                }
            },
            barWidth: 5,
            barGap: '200%',
            label: {
                normal: {
                    show: true,
                    position: 'right',
                    color: '#fff',
                    fontSize: 12,
                    fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                    fontWeight: 'Normal',
                    distance: 10
                }
                
            }
        },
        {
            name: '',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#07AB68',
                    barBorderRadius: 1,
                    label: {
                        show: true,  
                        position: 'right',  
                        textStyle: {  
                            color: '#fff',
                            fontSize: 12,
                            fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                            fontWeight: 'Normal',
                            distance: 10  
                        }
                    }
                }
            },
            barWidth: 5,
            barCategoryGap: '180%'
        }
    ]
};
export const labelOverviewOption = {
    xAxis: {
        axisTick: {
            show: false 
        },
        axisLine: {
            show: false 
        },
        itemStyle: {
            normal: {
                color: '#5e6a80',
                fontSize: 12,
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                fontWeight: 'Normal'
            }
        },
    },
    yAxis: {
        show: false 
    },
    series: [
        {
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#4278FC',
                }
            },
            barWidth: 16,
            barBorderRadius: 10,
            barGap: '100%',
            label: {
                normal: {
                    show: true,
                    position: 'top',
                    color: '#A1A1A1',
                    fontSize: 10,
                    fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                    fontWeight: 'Normal',
                    distance: 10 
                }
                
            },
        },
        {
            type: 'bar',
            itemStyle: {
                normal: {
                    color: '#3FBC8D',
                    label: {
                        ...labelStyle,
                    }
                }
            },
            barWidth: 16,
            barCategoryGap: '10%',
        }
    ]
};
export const sceneOverviewBarOption = {
    ...gridStyle,
    series: [
        {
            type: 'bar',
            label: {
                show: true, 
                position: 'top', 
                textStyle: { 
                    color: '#A1A1A1',
                    fontSize: 10,
                    fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                    fontWeight: 'Normal',
                    distance: 10 
                },
            },
            itemStyle: {
                normal: {
                    color: function(params) {
                        return params;
                    }
                }
            },
            barWidth: 16,
            barCategoryGap: '10%',
            barMaxWidth: 16
        }
    ]
}
export const sceneOverviewPieOption = {
    color:['#4278FC', '#3FBC8D', '#FAA819'],
    title: [
        {
            text: '',
            top: '40%',
            left: 'center',
            textStyle: {
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                fontWeight: 'Normal',
                fontSize: 14,
                color: '#131416'
            }
        },
        {
            text: '',
            left: 'center',
            top: '50%',
            textStyle: {
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                fontSize: 24,
                fontWeight: 'bold',
                color: '#131416'
            },
        },
        {
            text: 'TB',
            left: 'center',
            top: '60%',
            textStyle: {
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                fontWeight: 'Normal',
                fontSize: 14,
                color: '#131416'
            },
        }
    ],
    tooltip: {
        trigger: 'item'
    },
    series: [
        {
            type: 'pie',
            radius: ['60%', '70%'],
            label: {
                show: false,
            },
            labelLine: {
                show: false
            }
        }
    ]
}
export const sceneScreenBarOption = {
    grid : {
        left: '5%',
        top: '6%',
        bottom: '1%'
    },
    xAxis: {
        show: false
    },
    yAxis: {
        type: 'category',
        position: 'left',
        offset: -8,
        axisLabel: {
            color: '#fff',
            fontSize: 11,
            fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
            fontWeight: 'Normal',
            textStyle: {
                align: 'left',

            }
        },
        axisLine: {
            show: false
        }
    },
    series: [
        {
            type: 'bar',
            itemStyle: {
                color: 'rgba(0,0,0,0)'
            },
            barWidth: 4,
            barGap: '300%',
            showBackground: true,
            backgroundStyle: {
                color: 'rgba(255,255,255,0)'
            }
        },
        {
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function(params) {
                        return params;
                    },
                    barBorderRadius: 1,
                    label: {
                        show: true,                   
                        position:[290,-15],
                        textStyle: {  
                            color: '#fff',
                            fontSize: 12,
                            fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                            fontWeight: 'Normal',
                            
                        }
                    }
                }
            },
            barWidth: 4,
            showBackground: true,
            backgroundStyle: {
                color: 'rgba(255,255,255,0.1)'
            }
        }
        
    ]
}
export const trainOverviewOption = {
    title: [
        {
            text: '',
            top: '5%',
            bottom: '10%',
            left: '22.5%',
            textStyle: {
                color: '#131416',
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                fontWeight: 'Normal',
                fontSize: 14,
            }
        },
        {
            text: '',
            top: '5%',
            bottom: '10%',
            left: '92%',
            textStyle: {
                color: '#4278fc',
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
                fontWeight: 'Normal',
                fontSize: 14,
            }
        }
    ],
    grid: {
        right: '2%',
        left: '25%'
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        splitLine: {
        show: true,
            lineStyle: {
                color: '#F5F5F5'
            }
        },
        axisLine: {
            lineStyle: {
                color: '#fff',
                opacity: 0.6
            }
        },
        axisLabel: {
            ...fontStyle,
        },
        itemStyle: {
            normal: {
                ...fontStyle,
            }
        },
        axisTick: {
            show: false
        }
    },
    yAxis: {
        min: 0,
        max: 100,
        minInterval: 25,
        ...yAxisStyle,
        axisLine: {
            show: false
        },
        axisLabel: {
            fontSize: 10,
            fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal',
            fontWeight: 'Normal',
            color: '#A1A1A1',
            margin: 25
        },
        axisTick: {
            show: false
        },
    },
    series: [
        {
            type: 'line',
            smooth: true,
            symbol: 'circle',
            symbolSize: 6,
            itemStyle: {
                color: '',
                borderColor: '#fff',
                borderWidth: 1,
            },
            lineStyle: {
                width: 1,
            },
            areaStyle: {
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                    {
                        offset: 0,
                        color: '',
                    },
                    {
                        offset: 1,
                        color: '',
                    }
                ])
            }
        }
    ]
};
export const trainScreenOption = {
    title: [
        {
            text: '',
            top: '35%',
            bottom: '0%',
            left: '-2.5%',
            textStyle: {
                color: '#fffa',
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal' ,
                fontWeight: 'Normal',
                fontSize: 10,
                opacity: 0.6,
            }
        },
        {
            text: '',
            top: '35%',
            bottom: '0%',
            left: '72%',
            textStyle: {
                color: '',
                fontFamily: 'Microsoft YaHei, Microsoft YaHei-Normal' ,
                fontWeight: 'Normal',
                fontSize: 10,
            }
        }
    ],
    grid: { 
        right: '16%',
        left: '1%',
        bottom: '0%'
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        show: false,
    },
    yAxis: {
        ...yAxisStyle,
        ...axisStyle,
    },
    series: [
        {
            data: [820, 932, 901, 934, 1290, 1330, 1320, 1220],
            type: 'line',
            symbol: 'circle',
            smooth: true,
            symbolSize: 4,
            itemStyle: {
                color: '',
                lineStyle: {
                    color: ''
                }
            },
            areaStyle: {
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                    {
                        offset: 0,
                        color: ''
                    },
                    {
                        offset: 0.8,
                        color: ''
                    },
                ])
            }
        }
    ]
};
export const simulationOverviewOption = {
    ...gridStyle,
    series: [
      {
        type: 'bar',
        itemStyle: {
          normal: {
            color: '#4278FC',
            label: {
                ...labelStyle
            },
          }
        },
        barWidth: 16,
        barCategoryGap: '10%',
        barMaxWidth: 16
      }
    ]
}