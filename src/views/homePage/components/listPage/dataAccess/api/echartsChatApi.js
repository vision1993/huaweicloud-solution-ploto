import axios from 'axios';

// 待脱敏数据接口
export const importAnonymizeData = () => axios.get('monitor/original');
export const importAnonymizeGraphData = () => axios.get('monitor/original?display_field=graph_business');
// 标注数据图表接口
export const importLabelChatData = () => axios.get('monitor/annotate/details');
export const importLabelQueryData = params => axios.get('monitor/annotate/details?query_time=' + params);
// 场景数据饼图接口
export const importScenePieData = () => axios.get('monitor/scene');
// 场景数据柱状图接口
export const importSceneBarData = () => axios.get('monitor/scene/details');
export const importSceneQueryData = params => axios.get('monitor/scene/details?query_time=' + params);
// 训练数据图表接口
export const importTrainData = () => axios.get('monitor/train/details');
// 仿真数据图表接口
export const importSimulationChatData = () => axios.get('monitor/emulation/details');
export const importSimulationQueryData = params => axios.get('monitor/emulation/details?query_time=' + params);