import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/views/homePage/HomePage.vue'
import DevelopedPage from '@/views/homePage/components/listPage/developedPage.vue'
import AnonymizeData from '@/views/homePage/components/listPage/anonymizeDataPage.vue'
import SceneData from '@/views/homePage/components/listPage/sceneDataPage.vue'
import Overview from '@/views/homePage/components/listPage/overviewPage.vue'
import DataPacket from '@/views/homePage/components/detailPage/packetsPage.vue'
import SceneSnippet from '@/views/homePage/components/detailPage/sceneSnippetPage.vue'
import LoginPage from '@/views/loginPage/LoginPage.vue'
import DataScreen from '@/views/dataScreen/index.vue'
import Event from '@/views/homePage/components/listPage/eventPage.vue'
import Alarm from '@/views/homePage/components/listPage/alarmPage.vue'


Vue.use(Router)



const routes = [
  {
    path: '/',
    redirect: '/Login',
  },
  {
    path: '/index',
    name: '首页',
    component: HomePage,
    redirect: '/Overview',
    children: [
      {
        path: '/DevelopedPage',
        name: '待开发页面',
        component: DevelopedPage
      },
      {
        path: '/AnonymizeData',
        name: '脱敏数据',
        component: AnonymizeData
      },
      {
        path: '/SceneData',
        name: '场景数据',
        component: SceneData
      },
      {
        path: '/Overview',
        name: '总览页',
        component: Overview
      },
      {
        path: '/DataPacket',
        name: '数据包',
        component: DataPacket
      },
      {
        path: '/SceneSnippet',
        name: '场景片段名称',
        component: SceneSnippet
      },
      {
        path: '/Event',
        name: '事件列表',
        component: Event
      },
      {
        path: '/Alarm',
        name: '告警列表',
        component: Alarm
      },
    ]
  },
  {
    path: '/Login',
    name: '登录',
    component: LoginPage
  },
  {
    path: '/DataScreen',
    name: '数据大屏',
    component: DataScreen
  },
]

const router =  new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 全局路由前置守卫
router.beforeEach((to,from,next)=>{
  if (to.path==='/Login'){
    Vue.prototype.$stor.del('X-CSRFToken');
    return next()
  }
  if(to.path==='/ForgetPasswordPage'){
    return next();
  }
  let token = Vue.prototype.$stor.get('X-CSRFToken');
  if(!token){
     next('/')
  }
  next()
})

export default router
