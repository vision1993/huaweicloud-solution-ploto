import Vue from 'vue'
import VueI18n from 'vue-i18n'
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import zh from './zh-cn/zh'

Vue.use(VueI18n)


const i18n = new VueI18n({
  locale: 'zh_CN',
  messages: {
    zh_CN: {
      ...zh,
      ...zhLocale
    },
    en_US: {
      test: 'English',
      ...enLocale,
    }
  }
})
export default i18n;