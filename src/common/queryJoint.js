export function queryJoint(data){
  let queryArr = [];
  Object.keys(data).forEach(key=>{
    if(data[key] !== '' && data[key] !== null && data[key] !== undefined){
      if(Array.isArray(data[key])){
        if(data[key].length !== 0){
          queryArr.push(`${key}=${JSON.stringify(data[key])}`);
        }
      }
      else if(typeof(data[key]) == 'object'){
        if(Object.keys(data[key]).length !== 0){
          queryArr.push(`${key}=${JSON.stringify(data[key])}`);
        }
      }else {
        queryArr.push(`${key}=${data[key]}`);
      }
    }
  })
  return queryArr.join('&');
}