export function numFormat(value) {
  if (!value || typeof value !== 'number') return '0';
  var intPart = Number(value).toFixed(0);
  var intPartFormat = intPart.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
  return intPartFormat
}
