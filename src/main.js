import Vue from 'vue'
import App from './App.vue'
import ElementUI, { Message } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import i18n from './i18n'
import router from './router'
import './assets/css/global.css'
import axios from 'axios'
import { getCookie, setCookie, delCookie } from './common/tools'
import { numFormat } from './common/numFilter'
import { queryJoint } from './common/queryJoint'
// 自定义字体样式
import './assets/font/font.css'
import moment from 'moment'
import envConfig from '../config/env'
import JsonExcel from 'vue-json-excel'

// 数字过滤
Vue.prototype.numFormat = numFormat

// query参数拼接
Vue.prototype.queryJoint = queryJoint

Vue.filter('dataFormat', function (input, fmtstring) {
  return moment(input).format(fmtstring)
})

ElementUI.Dialog.props.lockScroll.default = false
Vue.prototype.$stor = {
  get: getCookie,
  set: setCookie,
  del: delCookie
}
// 配置请求的根路径
axios.defaults.withCredentials = true
axios.interceptors.request.use(
  (config) => {
    let token = Vue.prototype.$stor.get('X-CSRFToken')
    if (token && !Object.prototype.hasOwnProperty.call(config.headers, 'X-CSRFToken')) {
      config.headers['X-CSRFToken'] = token
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)
axios.interceptors.response.use(
  (res) => {  
    return res
  },
  (err) => {
    if(err.response.status == 403 && err.response.data.detail ==  '身份认证信息未提供。'){
      Message.error('登录失效')
      router.push('/')
    }
    if(err.response.data.code){
      let code = err.response.data.code;
      code = code.split('.')[1];
      Message.error(i18n.t('errorCode['+ code +']'));
    }else return err

  }
)
axios.defaults.baseURL = envConfig.BASE_URL;
Vue.prototype.$url = axios
Vue.config.productionTip = false;  
Vue.use(ElementUI)
Vue.component('downloadExcel',JsonExcel);

ElementUI.i18n((key, value) => i18n.t(key, value));
i18n.locale = 'zh_CN';


new Vue({
  i18n,
  router,
  render: (h) => h(App)
  
}).$mount('#app')
