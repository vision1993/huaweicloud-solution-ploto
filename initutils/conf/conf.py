import sys
from ploto.conf.conf import obs_ak, obs_sk, obs_endpoint
sys.path.append("..")

# mysql数据库登录配置
DBHost = "***********"
DBPort = 3306
DBUser = "root"
DBPassword = "*************"
DBName = "ploto_test"

# obs接入配置
ObsAK = obs_ak
ObsSK = obs_sk
ObsEndpoint = obs_endpoint

# obs文件配置 objectname以逗号分割， 存储类型为标准、低频、归档， Times为文件填充的倍数， Dir默认为当前initutils目录
BucketName = "***********"
ObjectName = "scene,anonymize"
StorageClass = "STANDARD"
Times = 1
FileDir = "ploto_file"
