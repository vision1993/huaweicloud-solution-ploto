from __future__ import absolute_import
from enum import Enum


class StorageClassEnum(Enum):
    STANDARD = 0
    WARM = 1
    COLD = 2
