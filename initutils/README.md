# 概述

- 功能描述：帮助用户在构建ploto自动驾驶研发平台后，基于基准文件快速填充数据进行测试验证、体验平台
- 开发语言：Python
# 组织结构

```
├── _init_.py				
├── delete_test_data.py		--删除测试数据
├── download_obs.py			--从OBS下载基准数据
├── fill_db.py				--同步OBS新增数据至DB
├── fill_obs.py				--基于基准数据填充OBS
├── obs_enum.py
├── conf			
|  ├── conf.py				--配置文件
|  ├── init_log.txt			--填充数据生成日志
├── ploto_file				--基准数据存放目录
|  ├── anonymize			--脱敏数据存放目录
|  ├── scene				--场景数据存放目录
└── README.md
```


# initutils工具说明

## 配置文件

设置配置文件initutils/conf/conf.py中的数据库登录配置及OBS接入配置

```
import sys
from ploto.conf.conf import obs_ak, obs_sk, obs_endpoint
sys.path.append("..")

# mysql数据库登录配置
DBHost = "127.0.0.1"  		# 数据库地址
DBPort = 3306
DBUser = "root"
DBPassword = "********" 	# 数据库密码 
DBName = "ploto_test"

# OBS接入配置
ObsAK = obs_ak   			# 华为云账号的AK
ObsSK = obs_sk				# 华为云账号的SK
ObsEndpoint = obs_endpoint	# OBS的endpoint

BucketName = "ploto-test"	# OBS创建的桶名
ObjectName = "scene"		# 需要操作的数据模块
StorageClass = "STANDARD"	# OBS存储类型
Times = 1					# 需要填充的倍数
FileDir = "ploto_file"		# 基准文件路径
```

## 基准文件填充

基准文件默认在/initutils/ploto_file下，用户可自行替换，或从指定OBS桶中获取。

若从指定OBS桶中获取，需对initutils/conf/conf.py进行如下设置

| 字段          | 样例 | 描述     |
| :--------------- | :-------- | :------- |
| `BucketName`          | `ploto-test` | 桶名 |
| `ObjectName` | `anonymize,scene` | 需更新基准文件的数据模块名，以英文逗号分隔 |
| `FileDir` | `ploto_file` | 基准文件存放路径 |

配置完成后，运行./initutils/download_obs.py

## OBS测试数据填充

工具将从FileDir中获取基准文件，并以添加不同后缀名的形式生成OBS的测试数据。用户可以配置需要填充的数据模块，填充记录将保存在log文件中。

| 字段         | 样例              | 描述                                     |
| :----------- | :---------------- | :--------------------------------------- |
| `BucketName` | `ploto-test`      | 桶名                                     |
| `ObjectName` | `anonymize,scene` | 需要填充数据的数据模块名，以英文逗号分隔 |
| `FileDir`    | `ploto_file`      | 基准文件存放路径                         |

配置完成后，运行./initutils/fill_obs.py

## DB同步测试数据

工具将从log文件获取测试数据信息，从OBS获取部分字段信息，将增量数据同步至数据库。

运行./initutils/fill_db.py

## 测试数据清除

工具将根据init_log文件中测试数据信息，对OBS和DB中的测试数据进行删除，用户可修改conf.py中ObjectName字段控制需要清除的数据模块。

| 字段         | 样例              | 描述                                         |
| :----------- | :---------------- | :------------------------------------------- |
| `ObjectName` | `anonymize,scene` | 需要清除测试数据的数据模块名，以英文逗号分隔 |

配置完成后，运行./initutils/delete_test_data.py