module.exports = {
  // baseUrl: '/',
  publicPath: '/',

  outputDir: 'dist',
  assetsDir: 'assets',
  runtimeCompiler: true,
  productionSourceMap: true,
  parallel: true,

  css: {
    // 是否提取css 生产环境可以配置为 true
    extract: false
  },

  devServer: {
    host: 'localhost.huawei.com',
    port: '80',
    open: true
  },
  lintOnSave: false
}
