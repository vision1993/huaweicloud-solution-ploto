/**
 * Template version: 1.2.7
 * see http://vuejs-templates.github.io/webpack for documentation.
 */

const path = require('path')

module.exports = {
    dev: {
        // Paths
        assetsSubDirectory: 'static',
        assetsPublicPath: '/',
        proxyTable: {},
        headers: {
            origin: 'localhost.com:8080'
        },
        // host: 'www.ploto.com',
        useEslint: true
    }
}